import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-artist-component',
  templateUrl: './artist-component.component.html',
  styleUrls: ['./artist-component.component.scss']
})
export class ArtistComponentComponent implements OnInit {

  @Input() artist;
  @Output () deleteClickEmitter = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  deleteArtist(idArtist){
    this.deleteClickEmitter.emit(idArtist);
    }
}
