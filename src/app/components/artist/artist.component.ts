import { Component, OnInit } from '@angular/core';
import { ArtistService } from 'src/app/services/artist.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {

  artists = [];
  artist;
  constructor(private artistService:ArtistService) { }

  ngOnInit(): void {
    const artistService=this.artistService.getArtists();
    this.artistService.getArtists().subscribe((artists:any)=>{
      this.artists=artists;
    })
  }

  deleteArtist(idArtist){
    this.artistService.deleteArtist(idArtist).subscribe((artist:any)=>{
      this.artist=artist;
    })
  
  }

}

