import { Component, OnInit } from '@angular/core';
import { AlbumService } from 'src/app/services/album.service';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit {

  albums = [];
  album;
  constructor(private albumService: AlbumService) { }


  ngOnInit(){

    const albumService=this.albumService.getAlbums();
    this.albumService.getAlbums().subscribe((albums:any)=>{
      this.albums=albums;
    })

  }

  deleteAlbum(idAlbum){
    this.albumService.deleteAlbum(idAlbum).subscribe((album:any)=>{
      this.album=album;
    });
  
}

filterAlbums($event){
  this.albumService.getAlbumFiltered($event.artistId).subscribe((res:any)=>{
    this.albums=res.results;
});
}

}