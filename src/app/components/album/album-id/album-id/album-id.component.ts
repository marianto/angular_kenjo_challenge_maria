import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlbumService } from 'src/app/services/album.service';

@Component({
  selector: 'app-album-id',
  templateUrl: './album-id.component.html',
  styleUrls: ['./album-id.component.scss']
})
export class AlbumIdComponent implements OnInit {

  album: any;
  @Output() deleteClickEmitter = new EventEmitter();
  constructor(private route: ActivatedRoute, private albumService:AlbumService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params =>{
      const idAlbum = params.get('idAlbum');
      this.albumService.getAlbumbyId(idAlbum).subscribe(album =>{
        this.album = album;
      });
    })
  }

  
  
  
}
