import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumIdComponent } from './album-id.component';

describe('AlbumIdComponent', () => {
  let component: AlbumIdComponent;
  let fixture: ComponentFixture<AlbumIdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumIdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
