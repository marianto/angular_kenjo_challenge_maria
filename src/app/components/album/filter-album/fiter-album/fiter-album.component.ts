import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-fiter-album',
  templateUrl: './fiter-album.component.html',
  styleUrls: ['./fiter-album.component.scss']
})
export class FiterAlbumComponent implements OnInit {

  formAlbumSearch;
  @Output() filterEmitter = new EventEmitter()
  constructor(private formBuilder:FormBuilder) { }

  ngOnInit(): void {
    this.formAlbumSearch=this.formBuilder.group({
      genre: [''],
    });
  }

  filter(){
    this.filterEmitter.emit();
    this.filterEmitter.emit(this.formAlbumSearch.value);
  }
}
