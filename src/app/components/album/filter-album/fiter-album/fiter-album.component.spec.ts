import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiterAlbumComponent } from './fiter-album.component';

describe('FiterAlbumComponent', () => {
  let component: FiterAlbumComponent;
  let fixture: ComponentFixture<FiterAlbumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FiterAlbumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiterAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
