import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ArtistService } from 'src/app/services/artist.service';
import { take } from "rxjs/operators";
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-artist',
  templateUrl: './create-artist.component.html',
  styleUrls: ['./create-artist.component.scss']
})
export class CreateArtistComponent implements OnInit {

  public formGroupArtist;
  constructor(private formBuilder:FormBuilder, private artistService:ArtistService,private httpClient:HttpClient, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.formGroupArtist = this.formBuilder.group({
      name: ['name',[Validators.required]],
      photoUrl: ['photoUrl'],
      
    })
  }

  createArtist(){
    console.log(this.formGroupArtist.value);
    const httpOptions ={
    headers: new HttpHeaders({
     'Content-Type':'application/json'
      })
    }
     return this.httpClient.post(environment.url + '/artist', this.formGroupArtist.value, httpOptions).pipe(take(1)).subscribe(() => {});
       
   }

}
