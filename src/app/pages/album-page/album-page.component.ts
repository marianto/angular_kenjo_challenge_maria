import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { AlbumService } from 'src/app/services/album.service';

@Component({
  selector: 'app-album-page',
  templateUrl: './album-page.component.html',
  styleUrls: ['./album-page.component.scss']
})
export class AlbumPageComponent implements OnInit {

@Input () albums = [];
@Output() deleteClickEmitter = new EventEmitter();
  constructor(private albumService:AlbumService) { }

  ngOnInit(): void {
  }

  deleteAlbum(idAlbum){
    this.deleteClickEmitter.emit(idAlbum);
  }
  
}
