import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Album } from '../models/album.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AlbumService {



  constructor(private httpClient:HttpClient) { }


  getAlbums(){
    return this.httpClient.get(environment.url + '/albums/all');
  }

  getAlbumbyId(idAlbum){
    return this.httpClient.get(environment.url + '/album/' + idAlbum);
  }

  deleteAlbum(idAlbum){
    return this.httpClient.delete(environment.url + '/album/' + idAlbum)
  }

  updateAlbum(idAlbum, body){
    return this.httpClient.put(environment.url + '/album/' + idAlbum, body)
  }

  getAlbumFiltered(filter){
    return this.httpClient.get(environment.url + '/album/' + filter)
  }

  createAlbum(album){
    
    const httpOptions ={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      })
    }
    return this.httpClient.post(environment.url + '/album', album,httpOptions);
    
    
  }
  
  
}
