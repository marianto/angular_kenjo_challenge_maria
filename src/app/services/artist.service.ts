import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {


  constructor(private httpClient:HttpClient) { }


  createArtist(artist){
    
    const httpOptions ={
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      })
    }
    return this.httpClient.post(environment.url + '/artist', artist,httpOptions);
    
   
  }

  getArtists(){
    return this.httpClient.get(environment.url + '/artists/all');
  }


  getArtistyId(idArtist){
    return this.httpClient.get(environment.url + '/artist/' + idArtist);
  }

  deleteArtist(idArtist){
    return this.httpClient.delete(environment.url + '/artist/' + idArtist)
  }


}
